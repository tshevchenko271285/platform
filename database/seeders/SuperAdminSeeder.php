<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SuperAdminSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::updateOrCreate(
            [
                'email' => env('SUPER_ADMIN_EMAIL', 'example@example.com'),
            ],
            [
                'name' => 'Admin',
                'lastname' => 'Super',
                'phone' => env('SUPER_ADMIN_PHONE'),
                'password' => Hash::make(env('SUPER_ADMIN_PASSWORD', '123123')),
            ]
        );
    }
}
