<?php

return [
    'create_user' => 'Create User',
    'id' => 'ID',
    'name' => 'Name',
    'lastname' => 'lastname',
    'email' => 'Email',
    'phone' => 'Phone',
    'country' => 'Country',
    'city' => 'City',
    'postcode' => 'Postcode',
    'address' => 'Address',
    'password' => 'Password',
    'password_confirm' => 'Password Confirmation',
    'created' => 'Created',
    'action' => 'Action',
    'title' => 'User',
    'active' => 'Active',
    'index_title' => 'Users',
    'create_title' => 'Create user',
    'edit_title' => 'Edit user',
];
