<?php

return [
    'index_title' => 'Roles',
    'create_title' => 'Create role',
    'edit_title' => 'Edit role',
    'toggle_all_roles' => 'Toggle all roles',
];
