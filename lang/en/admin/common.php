<?php

return [
    'edit' => 'Edit',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'delete' => 'Delete',
    'create' => 'Create',
    'back' => 'Back',
    'no_data' => 'No data found',
    'update_successful' => 'Update Successful',
    'yes' => 'Yes',
    'no' => 'No',

    // Field names
    'field_title' => 'Title',
    'field_toggle_all' => 'Toggle all',
    'label_roles' => 'Roles',
];
