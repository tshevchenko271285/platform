<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\InvokableRule;

class PhoneNumber implements InvokableRule
{
    /**
     * Pattern to test in the regexp
     */
    const PHONE_PATTER = '/^\+\d{10,13}$/';

    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        if (!preg_match(self::PHONE_PATTER, $value)) {
            $fail('validation.custom.phone.regex')->translate();
        }
    }
}
