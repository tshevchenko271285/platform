<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use \DB;
use \Hash;
use \Auth;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return Renderable
     */
    public function edit(): Renderable
    {
        $user = Auth::user();

        $user->load('roles', 'customFields');
        $f = $user->custom_fields;

        return view('profile.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProfileRequest $request
     * @return RedirectResponse
     */
    public function update(ProfileRequest $request): RedirectResponse
    {
        try {
            $data = $request->validated();

            if (!empty($data['password'])) {
                $data['password'] = Hash::make($data['password']);
            } else {
                unset($data['password']);
            }

            $user = User::findOrFail(Auth::id());

            DB::beginTransaction();

            $user->update($data);

            if (!empty($data['custom_fields'])) {
                array_walk(
                    $data['custom_fields'],
                    fn ($v, $k) => $user->customFields()->updateOrCreate(
                        ['key' => $k],
                        ['value' => $v, 'field_type' => '_test']
                    ),
                );
            }

            DB::commit();

            return redirect()
                ->route('profile.edit')
                ->with('success', trans('common.update_successful'));
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()
                ->route('profile.edit')
                ->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return RedirectResponse
     */
    public function destroy(): RedirectResponse
    {
        Auth::user()->delete();

        Auth::logout();

        return redirect('/');
    }
}
