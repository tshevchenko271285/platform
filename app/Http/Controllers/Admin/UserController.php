<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Tshevchenko\Rbac\Models\RbacRole;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('admin.user.index', [
            'users' => User::with('roles')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        return view('admin.user.create', [
            'roles' => RbacRole::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return RedirectResponse
     */
    public function store(UserRequest $request): RedirectResponse
    {
        try {
            $data = $request->validated();
            $data['password'] = Hash::make($request['password']);

            $user = User::create($data);

            $user->roles()->sync($data['roles']);
        } catch (\Exception $e) {
            return redirect()
                ->route('admin.user.create')
                ->withErrors([$e->getMessage()]);
        }

        return redirect()->route('admin.user.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Renderable
     */
    public function edit(User $user): Renderable
    {
        $user->load('roles');

        return view('admin.user.edit', [
            'roles' => RbacRole::all(),
            'user' => $user,
            'activeRolesID' => $user->roles->pluck('id')->toArray(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(UserRequest $request, User $user): RedirectResponse
    {
        try {
            $data = $request->validated();

            if (!empty($data['password'])) {
                $data['password'] = Hash::make($data['password']);
            } else {
                unset($data['password']);
            }

            $user->update($data);

            $user->roles()->sync($data['roles'] ?? []);

            return redirect()
                ->route('admin.user.edit', $user)
                ->with('success', trans('common.update_successful'));
        } catch (\Exception $e) {
            return redirect()
                ->route('admin.user.edit', $user)
                ->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return RedirectResponse
     */
    public function destroy(User $user): RedirectResponse
    {
        $user->delete();

        return redirect()->route('admin.user.index');
    }
}
