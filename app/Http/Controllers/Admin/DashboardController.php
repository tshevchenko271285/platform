<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Support\Renderable;

class DashboardController extends Controller
{
    /**
     * Show the admin panel dashboard.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('admin.dashboard');
    }
}
