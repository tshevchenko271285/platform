<?php

namespace App\Http\Requests;

use App\Http\Requests\Admin\CommonRequest;
use App\Rules\PhoneNumber;
use Illuminate\Validation\Rules\Password;

class ProfileRequest extends CommonRequest
{
    protected function putRules(): array
    {
        $user = \Auth::user();

        return [
            'name' => ['required', 'string', 'max:255', 'min:2'],
            'lastname' => ['nullable', 'string', 'max:255', 'min:2'],
            'email' => ['required', 'email:strict', 'max:255', 'unique:users,email,' . $user->id],
            'phone' => ['required', new PhoneNumber],
            'password' => ['nullable', 'string', 'confirmed', Password::min(8)->numbers()->letters()],
            'custom_fields' => ['nullable', 'array'],
        ];
    }
}
