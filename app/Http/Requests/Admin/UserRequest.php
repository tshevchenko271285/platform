<?php

namespace App\Http\Requests\Admin;

use App\Rules\PhoneNumber;
use Illuminate\Validation\Rules\Password;

class UserRequest extends CommonRequest
{
    protected function postRules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255', 'min:2'],
            'lastname' => ['nullable', 'string', 'max:255', 'min:2'],
            'email' => ['required', 'email:strict', 'max:255', 'unique:users,email'],
            'phone' => ['required', new PhoneNumber],
            'password' => ['required', 'string', 'confirmed', Password::min(8)->numbers()->letters()],
            'roles' => ['array'],
        ];
    }

    protected function putRules(): array
    {
        $user = $this->route('user');

        return [
            'name' => ['required', 'string', 'max:255', 'min:2'],
            'lastname' => ['nullable', 'string', 'max:255', 'min:2'],
            'email' => ['required', 'email:strict', 'max:255', 'unique:users,email,' . $user->id],
            'phone' => ['required', new PhoneNumber],
            'password' => ['nullable', 'string', 'confirmed', Password::min(8)->numbers()->letters()],
            'roles' => ['array'],
        ];
    }
}
