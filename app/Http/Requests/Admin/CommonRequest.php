<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

abstract class CommonRequest extends FormRequest
{
    const RULES_METHOD_POSTFIX = 'Rules';

    protected bool $authorize = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->authorize;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        try {
            $methodName = strtolower($this->method()) . self::RULES_METHOD_POSTFIX;

            return $this->$methodName();
        } catch (\Throwable $e) {
            return [];
        }
    }

    protected function getRules(): array
    {
        return [];
    }

    protected function postRules(): array
    {
        return [];
    }

    protected function putRules(): array
    {
        return [];
    }

    protected function patchRules(): array
    {
        return [];
    }

    protected function deleteRules(): array
    {
        return [];
    }
}
