@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto mb-5">
                @include('admin.user.particles.form-header', [
                    'title' => __('admin/user.edit_title'),
                ])
            </div>
        </div>

        <form class="form-horizontal" action="{{ route('profile.update') }}" method="post">
            {{ method_field('PUT') }}

            {{csrf_field()}}

            @include('profile.particles.form', [
                'user' => $user,
            ])

            <hr/>

            <input type="submit" class="btn btn-primary" value="{{ trans('admin/common.save') }}">
        </form>
    </div>
@endsection
