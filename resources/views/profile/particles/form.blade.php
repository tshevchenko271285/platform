@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

@if($user->custom_fields->has('test'))
    {{ $user->custom_fields->get('test')->view }}
@endif

<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <input
                type="text"
                name="custom_fields[test]"
                class="form-control"
                value="{{ old('custom_fields[test]', $user->custom_fields->has('test') ? $user->custom_fields->get('test')->value : '') }}"
            />
        </div>

        <div class="form-group mb-3">
            <input
                type="text"
                name="custom_fields[test_field]"
                class="form-control"
                value="{{ old('custom_fields[test_field]', $user->custom_fields->has('test_field') ? $user->custom_fields->get('test_field')->value : '') }}"
            />
        </div>
    </div>
    <div class="col-md-6"></div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="row">
            <div class="col-md-6 mb-3">
                <label for="">{{ trans('admin/user.name') }} *</label>

                <input
                    type="text"
                    class="form-control"
                    name="name"
                    placeholder="{{ trans('admin/user.name') }}"
                    value="{{ old('name', !empty($user) ? $user->name : "") }}"
                />
            </div>

            <div class="col-md-6 mb-3">
                <label for="">{{ trans('admin/user.lastname') }}</label>

                <input
                    type="text"
                    class="form-control"
                    name="lastname"
                    placeholder="{{ trans('admin/user.lastname') }}"
                    value="{{ old('lastname', !empty($user) ? $user->lastname : '') }}"
                />
            </div>

            <div class="col-md-6 mb-3">
                <label for="">{{ trans('admin/user.email') }} *</label>

                <input
                    type="email"
                    class="form-control"
                    name="email"
                    placeholder="{{ trans('admin/user.email') }}"
                    value="{{ old('email', !empty($user) ? $user->email : "") }}"
                />
            </div>

            <div class="col-md-6 mb-3">
                <label for="">{{ trans('admin/user.phone') }} *</label>

                <input
                    type="tel"
                    class="form-control"
                    name="phone"
                    placeholder="{{ trans('admin/user.phone') }}"
                    value="{{ old('phone', !empty($user) ? $user->phone : "") }}"
                />
            </div>

            <div class="col-md-6 mb-3">
                <label for="">{{ trans('admin/user.password') }}</label>

                <input type="password" class="form-control" name="password">
            </div>

            <div class="col-md-6 mb-3">
                <label for="">{{ trans('admin/user.password_confirm') }}</label>

                <input type="password" class="form-control" name="password_confirmation">
            </div>
        </div>
    </div>

    @if($user->roles)
        <div class="col-lg-4">
            <label class="form-label">{{ __('admin/common.label_roles') }}</label>

            @foreach($user->roles as $role)
                <label class="form-check">
                    <input
                        class="form-check-input"
                        type="checkbox"
                        checked
                        disabled
                    />

                    {{ $role->title }}
                </label>
            @endforeach
        </div>
    @endif
</div>
