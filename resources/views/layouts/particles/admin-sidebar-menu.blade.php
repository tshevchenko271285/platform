<nav class="sidebar-menu">
    <ul class="sidebar-menu__list">
        @if(\Auth::user()->hasAccess('admin.dashboard'))
            <li class="sidebar-menu-item">
                <a href="{{ route('admin.dashboard') }}" class="sidebar-menu-item__link">
                    <span class="sidebar-menu-item__icon">
                        <i class="fa-solid fa-house"></i>
                    </span>

                    <span class="sidebar-menu-item__text">
                        {{ __('Dashboard') }}
                    </span>
                </a>
            </li>
        @endif
    </ul>

    <ul class="sidebar-menu__list">
        @if(\Auth::user()->hasAccess('admin.user.index'))
            <li class="sidebar-menu-item">
                <a href="{{ route('admin.user.index') }}" class="sidebar-menu-item__link">
                    <span class="sidebar-menu-item__icon">
                        <i class="fa-solid fa-user"></i>
                    </span>

                    <span class="sidebar-menu-item__text">
                        {{ __('Users') }}
                    </span>
                </a>
            </li>
        @endif

        @if(\Auth::user()->hasAccess(config('rbac.route_name') . 'role.index'))
            <li class="sidebar-menu-item">
                <a href="{{ route(config('rbac.route_name') . 'role.index') }}" class="sidebar-menu-item__link">
                    <span class="sidebar-menu-item__icon">
                        <i class="fa-solid fa-users"></i>
                    </span>

                    <span class="sidebar-menu-item__text">
                        {{ __('Roles') }}
                    </span>
                </a>
            </li>
        @endif

        @if(\Auth::user()->hasAccess(config('rbac.route_name') . 'action.index'))
            <li class="sidebar-menu-item">
                <a href="{{ route(config('rbac.route_name') . 'action.index') }}" class="sidebar-menu-item__link">
                    <span class="sidebar-menu-item__icon">
                        <i class="fa-solid fa-handshake"></i>
                    </span>

                    <span class="sidebar-menu-item__text">
                        {{ __('Permissions') }}
                    </span>
                </a>
            </li>
        @endif
    </ul>
</nav>
