<div class="alert alert-danger" role="alert">
    {{ $field->field_type }} {{ __('doesnt exists') }}
</div>
