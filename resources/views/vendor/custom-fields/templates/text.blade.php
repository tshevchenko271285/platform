<label for="">{{ $field->label }}</label>

<input
    type="text"
    name="custom_fields[][value]"
    class="form-control"
    value="{{ old('custom_fields[test]', $field->value) }}"
/>
