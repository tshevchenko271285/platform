@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="card">
                <div class="card-header">{{ __('admin/user.index_title') }}</div>

                <div class="card-body">
                    <a href="{{ route('admin.user.create') }}" class="btn btn-primary">
                        {{ __('admin/common.create') }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 mx-auto">
            @if($users->count())
                <table class="mt-5 table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Username</th>
                            <th scope="col">E-Mail</th>
                            <th scope="col" width="100"></th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <a href="{{ route('admin.user.edit', $user->id) }}">
                                    {{ $user->full_name }}
                                </a>
                            </td>

                            <td>
                                <a href="{{ route('admin.user.edit', $user->id) }}">
                                    {{ $user->email }}
                                </a>
                            </td>

                            <td class="action">
                                <a
                                    href="{{ route('admin.user.edit', $user->id) }}"
                                    class="btn btn-sm btn-outline-primary"
                                >
                                    <i class="fa-solid fa-user-pen"></i>
                                </a>

                                <form
                                    onsubmit="return confirm('Delete?')"
                                    action="{{ route('admin.user.destroy', $user) }}"
                                    method="post"
                                    class="d-inline-block"
                                >
                                    @csrf
                                    @method('DELETE')
                                    <button
                                        class="btn btn-sm btn-outline-danger"
                                        type="submit"
                                        title="{{ trans('admin/common.delete') }}"
                                    >
                                        <i class="fa-regular fa-trash-can"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>
@endsection
