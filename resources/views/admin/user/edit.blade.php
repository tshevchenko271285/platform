@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto mb-5">
                @include('admin.user.particles.form-header', [
                    'title' => __('admin/user.edit_title'),
                ])
            </div>
        </div>

        <form class="form-horizontal" action="{{ route('admin.user.update', $user) }}" method="post">
            {{ method_field('PUT') }}

            {{csrf_field()}}

            @include('admin.user.particles.form', [
                'user' => $user,
            ])

            <hr/>

            <a href="{{ route('admin.user.index') }}" class="btn btn-default">
                {{ trans('admin/common.cancel') }}
            </a>

            <input type="submit" class="btn btn-primary" value="{{ trans('admin/common.save') }}">

        </form>
    </div>
@endsection
