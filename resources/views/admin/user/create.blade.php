@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto mb-3">
                @include('admin.user.particles.form-header', [
                    'title' => __('admin/user.create_title'),
                ])
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 mx-auto">
                <form class="form-horizontal" action="{{ route('admin.user.store') }}" method="post">
                    {{ csrf_field() }}

                    @include('admin.user.particles.form')

                    <hr/>

                    <a href="{{ route('admin.user.index') }}" class="btn btn-default">
                        {{ trans('admin/common.cancel') }}
                    </a>

                    <input type="submit" class="btn btn-primary" value="{{ trans('admin/common.save') }}">
                </form>
            </div>
        </div>
    </div>
@endsection
