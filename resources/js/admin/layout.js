/**
 * Toggle all checkboxes
 */
window.addEventListener('load', function () {
    const MAIN_CHECKBOX_SELECTOR = '[data-toggle-all-checkboxes]';
    const MAIN_DATASET_NAME = 'toggleAllCheckboxes';

    document.querySelectorAll(MAIN_CHECKBOX_SELECTOR).forEach(mainCheckbox => {
        const targetSelector = mainCheckbox.dataset[MAIN_DATASET_NAME];

        mainCheckbox.addEventListener('change', () =>
            document.querySelectorAll(targetSelector)
                .forEach(i => i.checked = mainCheckbox.checked)
        );

        document.addEventListener('change', (e) => {
            if (!e.target.closest(targetSelector)) return;

            const falseSelector = targetSelector + ':not(:checked)';
            const falseCheckboxes = document.querySelector(falseSelector);

            mainCheckbox.checked = !falseCheckboxes;
        });
    });
});
